package com.example.deepanshu.gestures_start;

/**
 * Created by Deepanshu on 10/12/2015.
 */
public interface Conventions {
    int INSIDE_COLOR=1,BOUNDARY_COLOR=2,OUTSIDE_COLOR=3,MIN_ROWS=0,MAX_ROWS=5,MIN_COLS=0,MAX_COLS=5;
    String INSIDE_CIRCLE="Inside The Circle",OUTSIDE_CIRCLE="Outside The Circle",BOUNDARY_CIRCLE="On Bounadry of The Circle",INVALID_VALUES="Enter Correct Values!!!!!";
}
