package com.example.deepanshu.gestures_start;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StageFirst extends AppCompatActivity implements Conventions,View.OnTouchListener {
    GradientDrawable bgShape;
    View viewCircle;
    RelativeLayout relativeCircle;
    float radius;
    float xCenter;
    float yCenter;
    TextView onTouchStatus;
    Button btnextLevel;
Boolean flag=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage_first);
        init();
        setOnClick();
    }

    public void init() {
        relativeCircle = (RelativeLayout) findViewById(R.id.relative_circle);
        viewCircle = findViewById(R.id.view_circle);
        bgShape = (GradientDrawable) viewCircle.getBackground();
        onTouchStatus = (TextView) findViewById(R.id.on_touch_status);
        btnextLevel = (Button) findViewById(R.id.bt_next_level);
    }

    public void setOnClick() {
        relativeCircle.setOnTouchListener(StageFirst.this);
        btnextLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(StageFirst.this, StageSeond.class);
                startActivity(it);
                finish();
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        getCenter();

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                process(event.getX(), event.getY());
                break;
            case MotionEvent.ACTION_DOWN:
                process(event.getX(), event.getY());

                break;
            case MotionEvent.ACTION_MOVE:
                process(event.getX(), event.getY());

                break;
        }

        return true;
    }

    public void process(float currX, float currY) {
        double dist;
        dist = getDistance(currX, currY);
        if (dist < radius) {
            toggleColor(INSIDE_COLOR);
        } else if (dist == radius) {
            toggleColor(BOUNDARY_COLOR);
        } else {
            toggleColor(OUTSIDE_COLOR);
        }
    }

    public void toggleColor(int cursorAt) {
        switch (cursorAt) {
            case INSIDE_COLOR:
                if (!(onTouchStatus.getText().toString().equalsIgnoreCase(INSIDE_CIRCLE ))) {
                    onTouchStatus.setText(INSIDE_CIRCLE );
                    flag = !flag;
                }
                break;
            case BOUNDARY_COLOR:
                bgShape.setColor(Color.RED);
                onTouchStatus.setText(BOUNDARY_CIRCLE );
                break;
            case OUTSIDE_COLOR:
                onTouchStatus.setText(OUTSIDE_CIRCLE);
                break;
        }
        if (flag)
            bgShape.setColor(Color.BLUE);
        else
            bgShape.setColor(Color.BLACK);


    }

    public double getDistance(float xPos, float yPos) {
        double distance;
        distance = Math.sqrt(Math.pow((xPos - xCenter), 2) + Math.pow((yPos - yCenter), 2));
        return distance;
    }

    public void getCenter() {
        radius = (viewCircle.getBottom() + viewCircle.getTop()) / 2;
        xCenter = (viewCircle.getRight() + viewCircle.getLeft()) / 2;
        yCenter = (viewCircle.getBottom() + viewCircle.getTop()) / 2;
    }

}
