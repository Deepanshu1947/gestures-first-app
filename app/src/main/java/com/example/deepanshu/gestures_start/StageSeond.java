package com.example.deepanshu.gestures_start;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.example.deepanshu.gestures_start.adapters.GridAdapter;

import java.util.Arrays;

public class StageSeond extends AppCompatActivity implements Conventions, View.OnTouchListener, View.OnClickListener {
    GridView gridView;
    GradientDrawable bgShape;
    GridAdapter gridViewAdapter;
    View viewCircle;
    float radius;
    float xCenter;
    float yCenter;
    TextView onTouchStatus;
    int numRows = 2, numCols = 3;
    Button btNextLevel;
    Boolean[] flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage_seond);
        init();
        setOnClick();
    }

    public void init() {
        btNextLevel = (Button) findViewById(R.id.bt_next_level);
        gridView = (GridView) findViewById(R.id.grid_show);
        onTouchStatus = (TextView) findViewById(R.id.on_touch_status);
        float gridViewHeight = 750, gridViewWidth = 550;
        flag = new Boolean[numRows * numCols];
        Arrays.fill(flag, false);
        gridViewAdapter = new GridAdapter(StageSeond.this, numRows, numCols, gridViewHeight, gridViewWidth);

    }

    public void setOnClick() {
        btNextLevel.setOnClickListener(StageSeond.this);
        gridView.setNumColumns(numCols);
        gridView.setAdapter(gridViewAdapter);
        gridViewAdapter.notifyDataSetChanged();
        gridView.setOnTouchListener(StageSeond.this);

    }

    public void process(float currX, float currY, int position) {
        double dist;
        dist = getDistance(currX, currY);
        if (dist < radius) {
            toggleColor(INSIDE_COLOR, position);
        } else if (dist == radius) {
            toggleColor(BOUNDARY_COLOR, position);
        } else {
            toggleColor(OUTSIDE_COLOR, position);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float currentXPosition = event.getX();
        float currentYPosition = event.getY();
        int position = gridView.pointToPosition((int) currentXPosition, (int) currentYPosition);
        if (position < 0) {
            onTouchStatus.setText(OUTSIDE_CIRCLE);
            return true;
        }
        setCircleInfo(position);
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                process(event.getX(), event.getY(), position);
                break;
            case MotionEvent.ACTION_DOWN:
                process(event.getX(), event.getY(), position);

                break;
            case MotionEvent.ACTION_MOVE:
                process(event.getX(), event.getY(), position);
                break;
        }
        return true;
    }

    public void setCircleInfo(int pos) {
        viewCircle = gridView.getChildAt(pos);
        bgShape = (GradientDrawable) viewCircle.getBackground();
        radius = (viewCircle.getWidth()) / 2;
        xCenter = (viewCircle.getRight() + viewCircle.getLeft()) / 2;
        yCenter = (viewCircle.getBottom() + viewCircle.getTop()) / 2;
    }

    public double getDistance(float xPos, float yPos) {
        double distance;
        distance = Math.sqrt(Math.pow((xPos - xCenter), 2) + Math.pow((yPos - yCenter), 2));
        return distance;
    }

    public void toggleColor(int cursorAt, int position) {
        switch (cursorAt) {
            case INSIDE_COLOR:
                if (!(onTouchStatus.getText().toString().equalsIgnoreCase(INSIDE_CIRCLE + position))) {
                    onTouchStatus.setText(INSIDE_CIRCLE + position);
                    flag[position] = !flag[position];
                }
                break;
            case BOUNDARY_COLOR:
                bgShape.setColor(Color.RED);
                onTouchStatus.setText(BOUNDARY_CIRCLE + position);
                break;
            case OUTSIDE_COLOR:
                onTouchStatus.setText(OUTSIDE_CIRCLE);
                break;
        }
        if (flag[position])
            bgShape.setColor(Color.BLUE);
        else
            bgShape.setColor(Color.BLACK);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_next_level:
                Intent it = new Intent(StageSeond.this, StageThird.class);
                startActivity(it);
                finish();
                break;
        }
    }
}
