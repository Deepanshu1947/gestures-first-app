package com.example.deepanshu.gestures_start.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.deepanshu.gestures_start.R;

/**
 * Created by Deepanshu on 10/8/2015.
 */
public class GridAdapter extends BaseAdapter {
    Context context;
    int numRows, numCols, width, height;

    public GridAdapter(Context context, int numRows, int numCols, float height, float width) {
        this.context = context;
        this.numRows = numRows;
        this.numCols = numCols;
        this.width = (int) (width / numCols);//width of an item
        this.height = (int) (height / numRows);//height of an item
    }


    @Override
    public int getCount() {
        return numCols * numRows;//number of items to be created
    }

    @Override
    public Object getItem(int position) {
        return null;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (height < width)
            width = height;
        else if (width < height)
            height = width;
        //to make the view circle rather than oval during custom number of rows and columns
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(width, height);//layout params of each item
        LayoutInflater li = LayoutInflater.from(context);
        convertView = li.inflate(R.layout.grid_column_layout, parent, false);
        convertView.setLayoutParams(params);
        return convertView;
    }
}
